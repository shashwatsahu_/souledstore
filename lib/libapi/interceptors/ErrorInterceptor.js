import ResponseInterceptor from './ResponseInterceptor';
import APIError from './APIError';


/**
 * This is the first interceptor and is responsible for intercepting the response and
 * based on the status code checks if the response is the error or success
 * and returns the respective success or error objects.
 */
export default class ErrorInterceptor extends ResponseInterceptor {
  async intercept(response) {
    try {
      const status = response.status;
      let payload = await response.text();
      if (status >= 200 && status < 300) {
        if (payload === '') {
          return payload;
        }
        return JSON.parse(payload);
      }
      payload = JSON.parse(payload);
      const error = new APIError('API response was not 200');
      if (Array.isArray(payload)) {
        error.msg = payload.length > 0 && payload[0].msg;
      } else {
        error.status = payload && payload.error && payload.error.status;
        error.msg = payload && payload.error && payload.error.msg;
        error.code = payload && payload.error && payload.error.code;
      }

      throw error;
    } catch (err) {
      throw err;
    }
  }
}
