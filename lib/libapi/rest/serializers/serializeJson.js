/**
 * Serialise the JSON body
 * @param object the request body.
 */
export const serializeJson = object => JSON.stringify(object);
