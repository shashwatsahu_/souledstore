import { createStackNavigator } from 'react-navigation';
import { LoginScreen } from '../../LoginScreen';
import {SideMenu} from '../../SideMenu/SideMenu.component';
import GetDrawerStack from './drawerStack';

function createNavigator() {
    const AuthStack = createStackNavigator(
        {
            LoginScreen: { screen: LoginScreen },
            drawerNavigation: { screen: GetDrawerStack },
        },
        {
            initialRouteName: 'drawerNavigation',
            // headerMode: 'none',
        }
    );
    return AuthStack;
}

export default createNavigator;
