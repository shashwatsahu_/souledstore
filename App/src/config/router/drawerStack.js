import React, { Component } from 'react';
import { View, StyleSheet, Easing, Animated } from 'react-native';
import { createDrawerNavigator, createStackNavigator, createAppContainer } from 'react-navigation';
import {SideMenu} from '../../SideMenu/SideMenu.component';
import { LoginScreen } from '../../LoginScreen';

// drawer stack
const drawerStack = (hasStandardPermission = false) => {
    return createDrawerNavigator({
     
      LoginScreen : createStackNavigator(
        {
            LoginScreen:{screen:LoginScreen}
        },
        {
            initialRouteName:'LoginScreen',
            headerMode:'none',
        }
    ),
    },
        {
            initialRouteName: 'LoginScreen',
            contentComponent: SideMenu,
            gesturesEnabled: false,
           drawerWidth: 175,
            headerForceInset: { top: 'never', bottom: 'never' },
            transitionSpec: {
                duration: 750,
                easing: Easing.out(Easing.poly(4)),
                timing: Animated.timing,
                useNativeDriver: true,
            },
        }
    );
};

class GetDrawerStack extends Component {

    render() {
        console.log('drawer');
        const DrawerNavigation = createAppContainer(drawerStack());
        return (
            <View style={styles.container}>
                <DrawerNavigation screenProps={SideMenu} />
            </View >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

export default GetDrawerStack;
