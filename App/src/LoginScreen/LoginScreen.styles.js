import { StyleSheet } from 'react-native';
import scaling from '../config/device/normalize';
import colors from '../../assets/colors';
const { normalize } = scaling;
const styles = StyleSheet.create({
    body: {
        backgroundColor: colors.mintGreen,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    sectionTitle: {
        fontSize: normalize(24),
        fontWeight: '600',
        color: colors.black,
    },
});

export default styles;
