/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {ScrollView, Text, View,TouchableOpacity} from 'react-native';
import styles from './LoginScreen.styles';

export default class LoginScreen extends Component{
    constructor(props){
        super(props)
    }
    render(){
        return (
            <View style={styles.body}>
                    <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
                <Text style={styles.sectionTitle}>Welcome FireLab</Text>
                </TouchableOpacity>
            </View>
        );
    }
}