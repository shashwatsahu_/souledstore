
import React, { Component } from 'react';
import configureStore from './src/config/store';
import createNavigator from './src/config/router';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { Spinner } from './src/widgets/Spinner';
import NavigationService from './src/widgets/Utils/NavigationService';
import { createAppContainer } from 'react-navigation';

export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            rehydrated: false,
            store: null,
            persistor: null,
        };
    }

    componentDidMount() {
        this.reHydratingStore();
    }

    reHydratingStore = async () => {
        const { store, persistor } = await configureStore();
        this.setState({ store, persistor, rehydrated: true });
    }

    saveNavigationRef = (ref) => {
        const navigationService = new NavigationService();
        navigationService.setNavigationRef(ref);
    }

    render() {
        const { store, persistor, rehydrated } = this.state;
        if (!rehydrated) { return <Spinner />; }
        const Navigation = createAppContainer(createNavigator());
        return (
            <Provider store={store}>
                <PersistGate loading={<Spinner />} persistor={persistor}>
                    <Navigation ref={this.saveNavigationRef} />
                </PersistGate>
            </Provider>
        );
    }
}
